import org.apache.maven.shared.osgi.*;

public class Ptest {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
	Maven2OsgiConverterTest tests = new Maven2OsgiConverterTest();
	System.out.println("Test GetBundleSymbolicName");
	tests.testGetBundleSymbolicName();
	System.out.println("Test GetBundleFileName");
	tests.testGetBundleFileName();
	System.out.println("Test GetVersion");
	tests.testGetVersion();
	System.out.println("Test ConvertVersionToOsgi");
	tests.testConvertVersionToOsgi();
        System.out.println("Tests Complete");
    }

}




